from math import log
from Bio import SeqIO

def ave_qual(quals):
    """Calculate average basecall quality of a read.
    Receive the integer quality scores of a read and return the average quality for that read
    First convert Phred scores to probabilities,
    calculate average error probability
    convert average back to Phred scale
    """
    if quals:
        return -10 * log(sum([10**(q / -10) for q in quals]) / len(quals), 10)
    else:
        return None

def extract_from_fastq(fq):
    """Extract quality score from a fastq file."""
    for rec in SeqIO.parse(fq, "fastq"):
        yield ave_qual(rec.letter_annotations["phred_quality"]))

if __name__ == "__main__":
    parser = argparse.ArgumentParser()
    parser.add_argument(
        'input_file', type=str, nargs='+',
        help='filepath to input fastq')

    args = parser.parse_args()

    avg_qual = 
    stdout.write(f"{output_path}\n")
